package com.groundgurus.day1;

/**
 *
 * @author pgutierrez
 */
public class Calculator {
    public int getSum(int firstNum, 
            int secondNum) {
        return firstNum + secondNum;
    }

    public int getDifference(int firstNum, int secondNum) {
        return firstNum - secondNum;
    }

}

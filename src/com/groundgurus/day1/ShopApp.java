package com.groundgurus.day1;

import com.groundgurus.day1.shop.Shop;
import com.groundgurus.day1.shop.WineShop;

/**
 *
 * @author pgutierrez
 */
public class ShopApp {

    public static void main(String[] args) {
        // Cake shop
        Shop cakeShop = new Shop("Red Ribbon",
                new String[]{
                    "Mocha Cake",
                    "Chocolate Cake"
                },
                "Wakwak",
                12);

        // Print Cake Shop Details
        cakeShop.printDetails();

        System.out.println();

        // Wine shop
        WineShop wineShop = new WineShop(false,
                "Juan's Winery",
                new String[]{
                    "Cheap Wine",
                    "Expensive Wine"
                },
                "Makati",
                -5);

        // Print Wine Shop Details
        wineShop.printDetails();
    }
    
    private void printAllShopDetails(Shop[] shops) {
        for (Shop shop : shops) {
            if (shop instanceof WineShop) {
                // then do this
            } else {
                shop.printDetails();
            }
        }
    }
}

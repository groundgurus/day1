package com.groundgurus.day1.shop;

/**
 *
 * @author pgutierrez
 */
public class WineShop extends Shop {

    boolean areMinorsAllowed;

    public WineShop(boolean areMinorsAllowed, String name, String[] items, String location, int numberOfEmployees) {
        super(name, items, location, numberOfEmployees);
        this.areMinorsAllowed = areMinorsAllowed;
    }

    @Override
    public void printDetails() {
        super.printDetails();
        System.out.println("Are Minors Allowed: "
                + areMinorsAllowed);
    }

}

package com.groundgurus.day1.shop;

/**
 *
 * @author pgutierrez
 */
public class Shop {
    private String name;
    private String[] items;
    private String location;
    private int numberOfEmployees;

    public Shop(String name, String[] items, String location, int numberOfEmployees) {
        this.name = name;
        this.items = items;
        this.location = location;
        this.numberOfEmployees = numberOfEmployees;
    }
    
    public void printDetails() {
        System.out.println("Name: " + name);
        System.out.println("Items: ");
        printItems();
        System.out.println("Location: " + location);
        System.out.println("Number of Employees: " 
                + numberOfEmployees);
    }
    
    public String findItemByName(String name) {
        return "";
    }
    
    public void printItems() {
        for (String item : items) {
            System.out.println(item);
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String[] getItems() {
        return items;
    }

    public void setItems(String[] items) {
        this.items = items;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getNumberOfEmployees() {
        return numberOfEmployees;
    }

    public void setNumberOfEmployees(int numberOfEmployees) {
        this.numberOfEmployees = numberOfEmployees;
    }
}

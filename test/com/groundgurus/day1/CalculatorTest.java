package com.groundgurus.day1;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author pgutierrez
 */
public class CalculatorTest {
    @Test
    public void testGetSum() {
        Calculator calculator = new Calculator();
        int actual = calculator.getSum(10, 20);
        
        assertEquals(30, actual);
    }
    
    @Test
    public void testGetDifference() {
        Calculator calculator = new Calculator();
        int actual = calculator.getDifference(10, 20);
        
        assertEquals(-10, actual);
    }
}
